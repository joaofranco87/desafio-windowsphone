﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace concrete.Converter
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            bool normal = true;
            if (parameter is string)
            {
                normal = !parameter.ToString().ToLower().Equals("reverse");
            }
            if (value is Boolean && (bool)value)
            {
                return normal ? Visibility.Visible : Visibility.Collapsed;
            }
            return normal ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (value is Visibility && (Visibility)value == Visibility.Visible)
            {
                return true;
            }
            return false;
        }
    }

}