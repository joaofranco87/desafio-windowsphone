﻿using System;
using System.Threading.Tasks;
using ViewModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace concrete
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MainPageVM _viewModel = new MainPageVM();

        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;

            this.DataContext = _viewModel;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
            await InitializeDribble();
        }

        private async Task InitializeDribble()
        {
            await _viewModel.InitializeAsync();
        }

        private async void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ItemDialog itemDialog = new ItemDialog((sender as ListView).SelectedItem as DribbbleItemVM);
            await itemDialog.ShowAsync();
        }

        private async void ItemClick(object sender, ItemClickEventArgs e)
        {
            ItemDialog itemDialog = new ItemDialog(e.ClickedItem as DribbbleItemVM);
            await itemDialog.ShowAsync();
        }

        private async void Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            await _viewModel.InitializeAsync();

        }
    }
}