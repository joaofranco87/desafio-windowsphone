﻿namespace Logic
{
    public class Helper
    {
        private static Helper current;
        public static Helper Current { get { return current; } private set { current = value; } }

        public Service Service { get; private set; }
        //User
        //Storage

        private Helper(IServiceProvider service)
        {   
            Service = new Service(service);
        }

        public static void Initialize(IServiceProvider service = null)
        {
            if (service == null)
                service = new DribbbleAPI();
            Current = new Helper(service);
        }
    }
}