﻿using Data;
using System.ComponentModel;

namespace ViewModel
{
    public class DribbbleItemVM : INotifyPropertyChanged
    {
        private string _title;
        public string Title { get { return _title; } set { _title = value; RaisePropertyChanged("Title"); } }

        private string _image;
        public string Image { get { return _image; } set { _image = value; RaisePropertyChanged("Image"); } }

        private string _description;
        public string Description { get { return _description; } set { _description = value; RaisePropertyChanged("Description"); } }

        private int _likes_count;
        public int LikesCount { get { return _likes_count; } set { _likes_count = value; RaisePropertyChanged("LikesCount"); } }

        private string _name;
        public string Name { get { return _name; } set { _name = value; RaisePropertyChanged("Name"); } }

        private string _avatar_url;
        public string AvatarUrl { get { return _avatar_url; } set { _avatar_url = value; RaisePropertyChanged("AvatarUrl"); } }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public DribbbleItemVM()
        {

        }

        public DribbbleItemVM(Shot shot)
        {
            AvatarUrl = shot.player.avatar_url;
            Name = shot.player.name;
            LikesCount = shot.likes_count;
            Description = shot.description;
            Title = shot.title;
            Image = shot.image_400_url;
        }

    }
}
