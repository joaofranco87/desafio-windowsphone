﻿using Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Data;
using System.Collections.ObjectModel;

namespace ViewModel
{
    public class MainPageVM : INotifyPropertyChanged
    {
        private int page = 1;
        private bool _isBusy;
        public bool IsBusy { get { return _isBusy; } set { _isBusy = value; RaisePropertyChanged("IsBusy"); } }
        private bool _isError;
        public bool IsError { get { return _isError; } set { _isError = value; RaisePropertyChanged("IsError"); } }
        private string _errorMessage;
        public string ErrorMessage { get { return _errorMessage; } set { _errorMessage = value; RaisePropertyChanged("ErrorMessage"); } }

        private ObservableCollection<DribbbleItemVM> _dribbbleList;
        public ObservableCollection<DribbbleItemVM> DribbbleList { get { return _dribbbleList; } set { _dribbbleList = value; RaisePropertyChanged("DribbbleList"); } }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        
        public MainPageVM()
        {
            DribbbleList = new ObservableCollection<DribbbleItemVM>();
        }

        public async Task InitializeAsync()
        {
            IsError = false;
            IsBusy = true;
            await GetDribbbles();
            IsBusy = false;
        }
        
        private async Task GetDribbbles()
        {
            try
            {
                var dribbleResponse = await Helper.Current.Service.GetPopularShots(page);
                ViewModelConverter(dribbleResponse);
                page++;
            }
            catch (Exception)
            {
                IsError = true;
                ErrorMessage = "Parece que você está sem internet! Por favor,verifique a sua conexão.";
            }
        }

        private void ViewModelConverter(RootDribbble dribbleResponse)
        {
            foreach (var shot in dribbleResponse.shots)
                DribbbleList.Add(new DribbbleItemVM(shot));
        }
    }
}
