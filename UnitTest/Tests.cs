﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System.Threading.Tasks;
using Logic;
using ViewModel;
using Data;

namespace UnitTest
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestHelper()
        {
            var serviceProvider = new MockClass();
            Helper.Initialize(serviceProvider);
            Assert.IsNotNull(Helper.Current.Service);
        }

        [TestMethod]
        public async Task TestViewModel()
        {
            var serviceProvider = new MockClass();
            Helper.Initialize(serviceProvider);
            var mainVM = new MainPageVM();
            await mainVM.InitializeAsync();
            Assert.IsNotNull(mainVM.DribbbleList);
        }

        [TestMethod]
        public void TestViewModelItem()
        {
            var shot = new Shot();
            shot.player = new Player();
            shot.player.avatar_url = "http://avatar.com.br";
            shot.player.name = "name";
            shot.likes_count = 10;
            shot.description = "description";
            shot.title = "title";
            shot.image_400_url = "http://image.com.br";

            var item = new DribbbleItemVM(shot);
            Assert.IsNotNull(item);
            Assert.AreEqual(item.Name, "name");
        }

        [TestMethod]
        public async Task TestGetDribbleShotsMock()
        {
            var serviceProvider = new MockClass();
            Logic.Service service = new Logic.Service(serviceProvider);
            var popularItems = await service.GetPopularShots(1);

            Assert.IsNotNull(popularItems);
            Assert.AreEqual(popularItems.shots.Length, 15);
        }

        [TestMethod]
        public async Task TestGetDribbleShots()
        {
            var serviceProvider = new DribbbleAPI();
            Logic.Service service = new Logic.Service(serviceProvider);
            var popularItems = await service.GetPopularShots(1);
            Assert.IsNotNull(popularItems);
        }
    }
}
